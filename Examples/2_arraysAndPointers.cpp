#include <iostream>

using namespace std;

// array variable name contains the address of the first element in the array
// so array name is a pointer to the first element of the array (holds the address of first element)

int main()
{
    double values[10];              // declare an array values with 10 elements
    double *pValue = values;        // define a pointer to the array

    cout << "value array address: " << values << endl;
    cout << "pValues: " << pValue << endl;

    int numbers[10];
    int *pNumbers = numbers;

    // this is an integer array so addresses should increase by 4 bytes
    for(int i = 0; i < 10; i++)
    {
        cout << "number address " << i << ": " << pNumbers + i << endl;
    }

    cout << endl;

    // same result
    for(int i = 0; i < 10; i++)
    {
        cout << "number address " << i << ": " << numbers + i << endl;
    }

    return 0;
}