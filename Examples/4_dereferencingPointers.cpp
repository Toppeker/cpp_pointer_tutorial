#include <iostream>

using namespace std;

// dereferencing means accessing the data inside that address

int main()
{
    float testScores[4];
    float sum = 0;
    float *pTestScores;

    for(int i = 0; i < 4; i++)
    {
        cout << "Enter your test score: " << endl;
        cin >> *(pTestScores + i);
        sum += *(pTestScores + i);
    }

    cout << "testScores: " << testScores << endl;
    cout << "Total for all students: " << sum << endl;
    cout << "Average for all students: " << sum / 4 << endl;

    return EXIT_SUCCESS;
}