#include <iostream>

using namespace std;

int main()
{
    int number = 240;   // define an integer variable
    int *numPtr;        // define an integer pointer
    numPtr = &number;   // assign the address to numPtr

    cout << "The address of number is: " << numPtr << endl;
    cout << "The value stored in the address is: " << *numPtr << endl << endl; 


    long longNumber = 55667;
    long *longPtr;
    longPtr = &longNumber;

    cout << "The address of longNumber is: " << longPtr << endl;
    cout << "The value stored in the address is: " << *longPtr << endl << endl;


    double dblNumber = 15.6;
    double *dblPtr;
    dblPtr = &dblNumber;

    cout << "The address of dblNumber is: " << dblPtr << endl;
    cout << "The value stored in the address is: " << *dblPtr << endl << endl;

    return 0;
}