#include <iostream>

using namespace std;
void writeCharArrayToConsole(const char *);

int main()
{
    char initial = 'P';
    char *pInitial = &initial;
    
    cout << "Memory address for initial P: " << pInitial << endl;   // what happens here?? some strange chars appear
    cout << "Memory address for initial P: " << (void *)pInitial << endl;
    cout << "Memory address for initial P: " << static_cast<void *> (pInitial) << endl;

    // Reason: char pointers mainly points to strings (char series)
    // when you try to print them as in first example, it tries to print the string (not the address in the memory)
    // thus, we need to cast it to print the address of char 'initial'

    // const char * pAnswer {"Yes"};

    // cout << pAnswer << endl;
    // writeCharArrayToConsole(pAnswer);

    return 0;
}

void writeCharArrayToConsole(const char * pChar)
{
    int i = 0;
    bool condition = true;
    while(condition)
    {
        if(*(pChar + i) != '\0')
        {
            cout << *(pChar + i);
            i += 1;
        }
        else
        {
            condition = false;
        }
    }

    cout << endl;
}