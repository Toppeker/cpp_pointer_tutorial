#include <iostream>

using namespace std;

float averageScore(float* scoreArray, int* count)
{
    float sum = 0;

    for(int i = 0; i < *count; i++)
    {
        sum += *(scoreArray + i);
    }
    float avg = sum / *count;

    *count += 2;    // what happens to numberOfExams in main ???
    return avg;
}

int main()
{
    float scores[3] {100.0, 88.5, 75.5};
    int numberOfExams = 3;
    float average = averageScore(scores, &numberOfExams);    // name of the array is address -> scores

    cout << "Average score: " << average << endl;
    cout << "New numberOfExams: " << numberOfExams << endl;
    
    return EXIT_SUCCESS;
}