#include <iostream>

using namespace std;

// memory allocation for different data types
// char, bool           1 byte
// short                2 bytes
// int, float           4 bytes
// double, long         8 bytes

int main()
{
    cout << "Size of boolean: " << sizeof(bool) << endl;
    cout << "Size of char: " << sizeof(char) << endl;

    cout << "Size of short: " << sizeof(short) << endl;

    cout << "Size of int: " << sizeof(int) << endl;
    cout << "Size of float: " << sizeof(float) << endl;

    cout << "Size of long: " << sizeof(long) << endl;
    cout << "Size of double: " << sizeof(double) << endl << endl;

    bool *pBool;
    char *pChar;
    int *pInt;
    double *pDouble;

    // memory allocation sizes of all pointers are same
    // because the values of pointers are always hexadecimal address values

    cout << "Size of boolean pointer is: " << sizeof(pBool) << endl;
    cout << "Size of char pointer is: " << sizeof(pChar) << endl;
    cout << "Size of int pointer is: " << sizeof(pInt) << endl;
    cout << "Size of double pointer is: " << sizeof(pDouble) << endl;

    return 0;
}