#include <iostream>

using namespace std;

int main()
{
    // Section 1
    int num = 10;
    int *pNum = &num;
    int **ppNum = &pNum;

    cout << "Value of num: " << num << endl;
    cout << "Value of num with dereferencing: " << *pNum << endl;
    
    cout << "Address of num: " << pNum << endl;
    cout << "Address of num with & operator: " << &num << endl;
    cout << "Address of num by dereferencing: " << *ppNum << endl;

    cout << "Address of pNum: " << &pNum << endl;
    cout << "Address of pNum: " << ppNum << endl;

    // // Section 2
    // // pointerArray is array of pointers to int (hold address values of integers)
    // int testScores[3] {100, 90, 80};
    // int * pointerArray[3];

    // for(int i = 0; i < 3; i++)
    // {
    //     pointerArray[i] = &(testScores[i]);
    //     cout << "Address: " << pointerArray[i] << " Address: " << &testScores[i] << "  Value: " << *(pointerArray[i]) << endl;
    //     cout << "Address: " << pointerArray[i] << "  Value: " << *(pointerArray + i) << "  " << **(pointerArray + i) << endl;
    //     // cout << "Address: " << pointerArray[i] << "  Value: " << **(pointerArray + i) << endl;
    // }

    // cout << pointerArray << endl;
    // cout << *(pointerArray) << endl;

    return EXIT_SUCCESS;
}