#include <iostream>

using namespace std;

// allocate memory at runtime rather than compile time
// advantage: flexible memory
// dynamic memory is identified by its address

// do not make -> memory leaks
// do not make -> dangling pointers

// memory-> stack and heap

// variables created at compile time -> stored in stack
// when variable is no longer used any more (when its scope ends), the stack is released

// inside heap memory -> dynamic variable creation
// at runtime -> new and delete operators

int main()
{
    // Section 1 - memory allocation for single int
    // int *pointer(new int(30));

    // cout << "Address of 30: " << pointer << endl;
    // cout << "Value: " << *pointer << endl;

    // delete pointer;

    // cout << "Address of 30: " << pointer << endl;
    // cout << "Value: " << *pointer << endl;


    // Section 2 - memory allocation for array

    // int* pArray(new int[3] {1, 3, 5});
    int *pArray;
    pArray = new int[3];
    for(int i = 0; i < 3; i++)
    {
        *(pArray + i) = i * 2 + 1;
    }
    
    for(int i = 0; i < 3; i++)
    {
        *(pArray + i) *= 2;
    }

    for(int i = 0; i < 3; i++)
    {
        cout << *(pArray + i) << endl;
    }

    delete [] pArray;   // pArray is called DANGLING POINTER if it is not assigned to NULL.

    cout << "Address (Dangling status): " << pArray << endl;    

    pArray = NULL;

    cout << "Address (After assigning null): " << pArray << endl; 

    // for(int i = 0; i < 3; i++)
    // {
    //     cout << *(pArray + i) << endl;
    // }

    return 0;
}